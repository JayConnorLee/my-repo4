
# 스크립트 이름: count_alphabet.py
# Input:
#     임의의 문자열
#     Top N
# Output:
#     개수 순서대로 Top N개의 알파벳 글자와 개수를 출력
# 제약:
#     대소문자를 구별하지 않음
#     알파벳 이외의 문자는 무시
#     개수가 동일한 알파벳은 알파벳 순으로 정렬



import argparse
import re


def get_args():
    parser = argparse.ArgumentParser();
    parser.add_argument("string", help="string to calculate");
    parser.add_argument("limit", help="limit to display");

    args = parser.parse_args();
    return args
  

def main():
    
    args = get_args()

    str1 = args.string
    limit = int(args.limit)
    
    lstr = list(re.sub(r'[^a-zA-Z]', '', str1.lower()))
    l = list(dict.fromkeys(list(str1)))

    newlist = [[x, str(lstr.count(x))] for x in l]
    newlist.sort(key=lambda x:(-int(x[1]), x[0]))

    newlist = newlist[:limit]
    result = "\n".join( [o[0] + ' ' +  o[1] for o in newlist] )

    print(result)



if __name__ == "__main__":
    main()


