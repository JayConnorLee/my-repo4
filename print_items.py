# 스크립트 이름: print_items.py

# Input
#     하나 이상의 아이템
#     --sorted 플래그 인자 (옵션)
# Output
#     아이템이 한 개만 입력된 경우: 아이템 그대로 출력
#     아이템이 두 개 입력된 경우: 아이템을 and로 연결
#     아이템이 세 개 이상 입력된 경우: 아이템을 콤마(,)로 연결하되 마지막 아이템은 and로 연결
#     --sorted 플래그가 주어진 경우: 아이템을 알파벳 순으로 정렬하여 출력
# 제약
#     동일한 아이템이 두 개 이상 입력된 경우 중복을 제거하여 한 번만 출력



import argparse


def get_args():
    parser = argparse.ArgumentParser();
    parser.add_argument("items", help="multiple items", nargs="+");
    parser.add_argument("--sorted", help="sort given items", action="store_true");

    args = parser.parse_args();
    return args
  

def main():
    
    args = get_args()
    l = list(dict.fromkeys(args.items))
    if args.sorted:
        l.sort()

    if len(l) == 1:  
        result = l[0]

    elif len(l) == 2:
        result = " and ".join(l) 

    elif len(l) >= 3:
        result = " and ".join( [ ", ".join(l[:-1]), l[-1]] )

    print(result)

if __name__ == "__main__":
    main()
